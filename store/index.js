export const state = () => ({
  auth: false
})

export const getters = {
  menu: (state) => state.auth
}

export const mutations = {
  SET_AUTH: (state, payload) => {
    state.auth = payload
  }
}

export const actions = {
  setAuth: ({ commit }, payload) => {
    commit('SET_AUTH', payload)
  }
}
